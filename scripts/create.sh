#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Correct usage: $0 <new-project-name>"
    exit 1
fi
cd $(dirname $0)
cd ../

TEMPLATE_PROJECT_DIR="$(pwd)"

cd ../

NAME=$1
DIR="$(pwd)"
PROJECT_DIR="$DIR/$NAME"

if [ -d "$NAME" ]; then
    echo "Project with name $NAME already exists"
    exit 2
fi

mkdir -p $PROJECT_DIR

cd $TEMPLATE_PROJECT_DIR
find . -type f \
  -not -iwholename "*/.git/*" \
  -not -iwholename "*/.gradle/*" \
  -not -iwholename "*/.idea/*" \
  -not -iwholename "*/scripts/*" \
  -not -iwholename "*/build/*" \
  -not -iwholename "*.iml" \
  -exec echo {} \; | awk -v dir="$PROJECT_DIR" '{ system("mkdir -p " dir "/$(dirname " $1 "); cp -r " $1 " " dir "/" $1) }'

cd $PROJECT_DIR
sed -i "s/voxele-template-plugin/$NAME/" settings.gradle